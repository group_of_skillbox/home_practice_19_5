#include<iostream>

using namespace std;

class Animal
{
public:

	virtual void Voice()
	{
		cout << "This message from the parent Class"<< endl;
	}
};

class Dog :public Animal
{
public:

	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat:public Animal
{
public:

	void Voice() override
	{
		cout << "Meow!" << endl;
	}
};

class Cow :public Animal
{
public:

	void Voice() override
	{
		cout << "Mooo!" << endl;
	}
};


int main()
{
	setlocale(LC_ALL, "ru");
	Cat Tom;
	Dog Rex;
	Cow Marta;
	
	Animal** arr = new Animal*[3]; // ������ ���������� ���� Animal

	arr[0] = new Cat[1];
	arr[1] = new Dog[1];
	arr[2] = new Cow[1];

	// ������ �������� �������-�����������
	arr[0][0] = Tom;
	arr[1][0] = Rex;
	arr[2][0] = Marta;

	// �������� �� ������ �������� ������� ����� Voice()
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 1; j++)
		{
			arr[i][j].Voice();
		}
	}
}